<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PaymentFormTest extends DuskTestCase
{
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://members.tushyraw.com/joinnow/')
                ->waitFor('#email')
                ->type('email', 'alexandroskalogiros@gmail.com')
                ->waitFor('#password')
                ->type('password', 'sadasdf12')
                ->clickLink('CONTINUE')
                ->pause(40000)//payment form takes forever to load
                ->assertSee('ROCKETPAY');
        });
    }
}
